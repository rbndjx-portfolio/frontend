export interface IEnv {
    backend?: {
        baseUrl: string;
        endpoints: {
            project: string;
            authentication: string;
        }
    }
}